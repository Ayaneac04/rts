﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LoadPointPosition : MonoBehaviour {

    public MovementController MCon;

    private ParticleSystem particle;

    // Use this for initialization
    void Start () {
        particle = this.GetComponent<ParticleSystem>();

        // ここで Particle System を停止する.
        particle.Stop();
    }
	
	// Update is called once per frame
	void Update () {

        if (MCon.CanMove)
        {
            particle.Play();
        }

        //マウスカーソルがUIと重なっているかの判定
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                // かぶさってるので処理キャンセル
                return;
            }


            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100))
            {
                transform.position = hit.point;
            }
        }

    }
}
