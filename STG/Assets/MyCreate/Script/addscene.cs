﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class addscene : MonoBehaviour {

    [SerializeField] private GameObject LCamera;

    public Canvas canvas;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private bool isLoaded = false;

    public void OnClick()
    {
        isLoaded = !isLoaded;
        if (isLoaded)
        {
            LCamera.SetActive(!LCamera.activeSelf);
            canvas.enabled = false;
            SceneManager.LoadSceneAsync("01", LoadSceneMode.Additive);
        }
        else
        {
            SceneManager.LoadSceneAsync("01");
            Resources.UnloadUnusedAssets();
        }
    }
}
