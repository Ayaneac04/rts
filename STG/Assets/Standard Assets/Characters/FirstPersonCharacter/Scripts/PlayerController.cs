using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private float m_StepInterval;      
        [SerializeField]private float m_FiringInterval = 0.3f;    //　弾を飛ばす間隔時間

        private Camera m_Camera;
        private bool m_Aim;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private bool m_reload;
        private GameObject m_gObject;                            //プレイヤー
        private Animator m_animator;                             //プレイキャラのアニメーション
        public GameObject[] m_Bullet;                            //弾
        private int m_BulletNo;                                  //バレットナンバー
        public Transform m_Muzzle;                               //発射口
        public float m_Speed = 500;                              //弾速
        private float m_ElapsedTime = 0f;                        //射撃間隔の時間
        private int m_AmmoCount = 30;                            //リロード時間
        public Text m_Ammo;
        private int m_Hp;

        // Use this for initialization
        private void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_FovKick.Setup(m_Camera);
            m_Aim = false;
			m_MouseLook.Init(transform , m_Camera.transform);
            m_gObject = GameObject.Find("Yuko_SchoolUniform_summer");
            m_animator = m_gObject.GetComponent<Animator>();
            m_Ammo.text = m_AmmoCount.ToString();
            m_Hp = 3;
            m_BulletNo = 0;
        }

        
        // Update is called once per frame
        private void Update()
        {
            RotateView();
           
            if (!m_CharacterController.isGrounded & m_PreviouslyGrounded)
            {
                m_MoveDir.y = 0f;
            }

            m_PreviouslyGrounded = m_CharacterController.isGrounded;

            if(CrossPlatformInputManager.GetButton("1"))
            {
                m_BulletNo = 0;
            }
            if (CrossPlatformInputManager.GetButton("2"))
            {
                m_BulletNo = 1;
            }
            if (CrossPlatformInputManager.GetButton("3"))
            {
                m_BulletNo = 2;

            }
            if (m_Aim = CrossPlatformInputManager.GetButton("Fire2"))
            {
                m_animator.SetBool("is_aim", true);
            }
            else
            {
                m_animator.SetBool("is_aim", false);
            }
            m_ElapsedTime += Time.deltaTime;
            if (m_ElapsedTime < m_FiringInterval)
            {
                return;
            }
            if (m_Aim&&CrossPlatformInputManager.GetButton("Fire1")&&!m_reload)
            {
                Shotting();
            }
            else
            {
                m_animator.SetBool("is_shotting", false);
            }
            if(CrossPlatformInputManager.GetButtonDown("Reload"))
            {
                m_AmmoCount = 30;
                m_reload = false;
            }
            if (CrossPlatformInputManager.GetButton("Cancel"))
            {
                GameEnd();
            }

            m_Ammo.text = m_AmmoCount.ToString();

        }

        private void FixedUpdate()
        {
            if (m_CharacterController.isGrounded)
            {
                m_MoveDir.y = -m_StickToGroundForce;
            }
            else
            {
                m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
            }
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);

            m_MouseLook.UpdateCursorLock();
        }

        private void GetInput()
        {
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxis("Vertical");

            m_Input = new Vector2(horizontal, vertical);


            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }
        }


        private void RotateView()
        {
            m_MouseLook.LookRotation (transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }

        //弾を撃つ処理
        private void Shotting()
        {

            transform.rotation = m_Camera.transform.rotation;

            m_animator.SetBool("is_shotting", true);
            // 弾丸の複製
            GameObject bullets = Instantiate(m_Bullet[m_BulletNo]) as GameObject;

            Vector3 force;

            force = gameObject.transform.forward * m_Speed;

            // Rigidbodyに力を加えて発射
            bullets.GetComponent<Rigidbody>().AddForce(force);

            // 弾丸の位置を調整
            bullets.transform.position = m_Muzzle.position;

            m_ElapsedTime = 0f;
            m_AmmoCount--;
            if (m_AmmoCount <= 0)
            {
                m_reload = true;
            }
        }

        //　ゲーム終了ボタンを押したら実行する
        public void GameEnd()
        {
            Application.Quit();
        }

        void OnCollisionEnter(Collision collision)
        {
            //衝突判定
            if (collision.gameObject.tag == "Enemy")
            {
                m_Hp--;
                if (m_Hp <= 0)
                {
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
